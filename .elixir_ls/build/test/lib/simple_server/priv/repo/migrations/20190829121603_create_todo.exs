defmodule SimpleServer.Repo.Migrations.CreateTodo do
  use Ecto.Migration

  def change do
      create table(:todo) do
        add :title, :string
        add :username, :string
        add :date, :date

        timestamps()
      end
    end

end
