use Mix.Config

config :simple_server, SimpleServer.Repo,
  database: "simple_server",
  username: "clem",
  password: "$Cl3m@200!",
  hostname: "localhost"

config :simple_server, ecto_repos: [SimpleServer.Repo]

