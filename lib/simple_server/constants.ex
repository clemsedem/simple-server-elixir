defmodule SimpleServer.Constants do
    @err_missing_title %{resp_code: "113", resp_desc: "Missing Title"}
    @err_missing_username %{resp_code: "114", resp_desc: "Missing Username"}
    @err_missing_date %{resp_code: "115", resp_desc: "Missing date"}
    @success_add %{resp_code: "111", resp_desc: "Todo Added successfully"}

    def err_missing_title, do: @err_missing_title
    def err_missing_username, do: @err_missing_username
    def err_missing_date, do: @err_missing_date
    def success_add, do: @success_add


#    clemenn
end