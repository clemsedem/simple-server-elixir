defmodule SimpleServer.RouterValids do

  alias SimpleServer.Constants


  def valid_title(nil), do: {:error, Constants.err_missing_title()}
  def valid_title(""), do: {:error, Constants.err_missing_title()}
  def valid_title(title) do
    if is_binary(title) do
      {:ok, title}
    else
      {:error, Constants.err_missing_title() }
    end
  end


  def valid_username(nil), do: {:error, Constants.err_missing_username()}
  def valid_username(""), do: {:error, Constants.err_missing_username()}
  def valid_username(username) do
    if is_binary(username) do
      {:ok, username}
    else
      {:error, Constants.err_missing_username }
    end
  end

  def valid_date(nil), do: {:error, Constants.err_missing_date() }
  def valid_date(""),  do: {:error, Constants.err_missing_date() }
  def valid_date(date) do
    if is_date?(date) do
      {:ok, date}
    else
      {:error, Constants.err_missing_date }
    end
  end



  def is_date?(date) do
    case Date.from_iso8601(date) do
      {:ok, _} -> true
      _ -> false
    end
  end


end