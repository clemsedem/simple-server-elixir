defmodule SimpleServer.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      SimpleServer.Repo,
      Plug.Adapters.Cowboy.child_spec(scheme: :http, plug: SimpleServer.Router, options: [port: 8085])
    ]

    opts = [strategy: :one_for_one, name: SimpleServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
