defmodule SimpleServer.Methods do
  alias SimpleServer.Repo
  alias SimpleServer.Todo
  alias SimpleServer.Constants

  def createTodo(title, username, date) do
      {:ok, date} = Date.from_iso8601(date)
      res = %Todo{title: title, username: username, date: date}
      |> Repo.insert
      res
  end



  def sayHello do
    %{
      response_code: "100",
      resp_message: "Hello buddy!, you are welcome to my zone!"
    }
  end

  def not_found do
    %{
      resp_code: "404",
      resp_desc: "Request not found!"
    }
  end



end
