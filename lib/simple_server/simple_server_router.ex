defmodule SimpleServer.Router do
  alias SimpleServer.Methods
  alias SimpleServer.Constants
  alias SimpleServer.RouterValids

  use Plug.Router
  use Plug.Debugger
  require Logger

  plug(Plug.Logger, log: :debug)
  plug(:match)
  plug(:dispatch)

# Routes
  get "/hello" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!( Methods.sayHello() ) )
  end

  post "/new_todo" do
    {:ok, params, conn} = read_body(conn)
    params = Poison.decode!(params)
    date = Map.fetch!(params, "date")
    title = Map.fetch!(params, "title")
    username = Map.fetch!(params, "username")

     with {:ok, title} <- RouterValids.valid_title(title),
          {:ok, username} <- RouterValids.valid_username(username),
          {:ok, date} <- RouterValids.valid_date(date)
     do
        case Methods.createTodo(title, username, date) do
          {:ok, struct} -> #inserted with success
            IO.puts "\nSuccessfully saved\n"
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(404, Poison.encode!(Constants.success_add()))
          {:error, changeset} ->
            IO.puts "\nFailed to save\n"
            IO.inspect changeset
        end
     else
      {:error, reason} ->
            conn
            |> send_resp(200, Poison.encode!(reason))
            |> halt
     end
#    conn
#    |> put_resp_content_type("application/json")
#    |> send_resp(201, Poison.encode!("Successfull"))
  end


  match _ do
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(404, Poison.encode!(Methods.not_found()))
  end


end
